import { shallowMount } from "@vue/test-utils";
import PxButton from "@/components/PxButton";

describe("PxButton Component Unit Test: ", () => {
  /* test("Is a vue instance", () => {
    const isLoading = true;
    const wrapper = shallowMount(PxButton, {
      propsData: { isLoading },
    });
    expect(wrapper.isVueInstance()).toBeTruthy();
  }); */

  test("Button when i clicked", () => {
    const isLoading = false;
    const event = jest.fn();
    const wrapper = shallowMount(PxButton, {
      propsData: { isLoading },
    });
    wrapper.vm.$on("custom-click", event);
    expect(event).toHaveBeenCalledTimes(0);

    wrapper.trigger('click')
    expect(event).toHaveBeenCalledTimes(1)

  });
});
