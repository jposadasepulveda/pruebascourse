import { shallowMount } from "@vue/test-utils";
import PxHeader from "@/components/PxHeader";
import { mount } from '@vue/test-utils';

describe("PxHeader Component Unit Test: ", () => {
  const links = [
    {
      title: "BTC",
      to: { name: "coin-detail", params: { id: "bitcoin" } },
    },
    {
      title: "ETH",
      to: { name: "coin-detail", params: { id: "ethereum" } },
    },
    {
      title: "XRP",
      to: { name: "coin-detail", params: { id: "ripple" } },
    },
  ];
  /* test("Is a vue instance", () => {
    const wrapper = shallowMount(PxHeader, {
      propsData: { links },
    });
    expect(wrapper.isVueInstance()).toBeTruthy();
  }); */

  test("Validate that All Cryptos in Link contain the Header", () => {
    const wrapper = mount(PxHeader, {
      propsData: { links },
    });
    expect(wrapper.html()).toContain('BTC');
  });
});
