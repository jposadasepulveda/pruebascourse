import api from "../../src/api"

describe("Test Api Functions", () => {
  test("Test getAssets", async () => {
    let asset;
    asset = await api.getAssets2()
    expect(asset[0].id).toBe('bitcoin');
  });


  test("Test get One Asset", async () => {
    let asset;
    asset = await api.getAsset2('bitcoin')
    expect(asset.id).toBe('bitcoin');
    expect(asset.symbol).toBe('BTC');
  });

  test("Test That the Asset History, contain the last 24 hours", async () => {
    let assetHistory;
    assetHistory = await api.getAssetHistory2('bitcoin')
    expect(assetHistory).toHaveLength(24);
  });
});
